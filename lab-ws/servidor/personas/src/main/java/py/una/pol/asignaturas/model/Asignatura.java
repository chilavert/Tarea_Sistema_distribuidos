package py.una.pol.asignaturas.model;

import java.io.Serializable;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement

public class Asignatura implements Serializable {
	int id;
	String nombre;
	int semestre;
	public Asignatura()
	{
		
	}
	
	
	public Asignatura(int id,String nombre, int semestre) {
        this.id=id;
		this.nombre = nombre;
		this.semestre = semestre;
	}
	
	

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getSemestre() {
		return semestre;
	}
	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	 
}
