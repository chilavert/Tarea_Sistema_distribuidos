package py.una.pol.asignaturas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Persona;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.asignaturas.service.AsignaturaService;

@Path("/asignaturas")
@RequestScoped

public class AsignaturaRESTService {
    @Inject
    private Logger log;

    @Inject
    AsignaturaService asignaturaService;
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Asignatura> listar() {
        return asignaturaService.seleccionar();
    }
    
    
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_XML)
    public Asignatura obtenerPorId(@PathParam("id") int id) {
        Asignatura p = asignaturaService.seleccionarPorID(id);
        if (p == null) {
        	log.info("obtenerPorId " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + id + " encontrada: " + p.getNombre());
        return p;
    }
    
    @GET
    @Path("/id")
    @Produces(MediaType.APPLICATION_XML)
    public Asignatura obtenerPorIdQuery(@QueryParam("id") int id) {
        Asignatura p = asignaturaService.seleccionarPorID(id);
        if (p == null) {
        	log.info("obtenerPorId " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + id + " encontrada: " + p.getNombre());
        return p;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Asignatura p) {

        Response.ResponseBuilder builder = null;

        try {
            asignaturaService.crear(p);
            // Create an "ok" response
            
            //builder = Response.ok();
            builder = Response.status(201).entity("Asignatura creada exitosamente");
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Asignatura p) {

        Response.ResponseBuilder builder = null;

        try {
            asignaturaService.actualizar(p);
            builder = Response.status(201).entity("Asignatura modificada exitosamente");
            
        } catch (SQLException e) 
        {
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    
    @DELETE
    @Path("/{id}")
    public Response borrar(@PathParam("id") int id)
    {      
 	   Response.ResponseBuilder builder = null;
 	   try {
 		   
 		   if(asignaturaService.seleccionarPorID(id) == null) {
 			   
 			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
 		   }else {
 			   asignaturaService.borrar(id);
 			   builder = Response.status(202).entity("Asignatura borrada exitosamente.");			   
 		   }
 		   

 		   
 	   } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

}
