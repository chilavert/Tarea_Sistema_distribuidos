package py.una.pol.aluasig.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.Bd;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.dao.PersonaDAO;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.asignaturas.dao.AsignaturaDAO;
import py.una.pol.aluasig.model.aluasig;

@Stateless
public class AluAsigDAO {
	
    @Inject
    private Logger log;
    
    public long relacionar(long cedula,long id) throws SQLException {
    	
    	PersonaDAO pdao=new PersonaDAO();
    	AsignaturaDAO adao=new AsignaturaDAO();
    	
    	Persona aux=pdao.seleccionarPorCedula(cedula);
    	Asignatura aux2=adao.seleccionarPorID(id);
    	
    	if(aux==null)
    	{
    		System.out.println("NO EXISTE EL ALUMNO");
    		return -1;
    	}else if(aux2==null)
    	{
    		System.out.println("NO EXISTE ESA ASIGNATURA");
    		return -1;
    	}    	
    	else{

        String SQL = "INSERT INTO aluasig(alumno, asignatura) "
                + "VALUES(?,?)";
 
        long a = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, cedula);
            pstmt.setLong(2, id);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        a = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return a;
    }}
 
	public List<Persona> listaAlumnos(long asignatura) {
		String SQL = "SELECT p.cedula, p.nombre, p.apellido FROM persona p inner join aluasig m on p.cedula=m.alumno inner join asignatura q on m.asignatura=q.id where q.id= ? ";
		
		List<Persona> lista = new ArrayList<Persona>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, asignatura);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Persona p = new Persona();
        		p.setCedula(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	
	public List<Asignatura> listaAsignaturas(long cedula) {
		String SQL = "SELECT p.id, p.nombre, p.semestre FROM asignatura p inner join aluasig m on p.id=m.asignatura inner join persona q on m.alumno=q.cedula where q.cedula= ?";
		
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, cedula);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Asignatura p = new Asignatura();
        		p.setId(rs.getInt(1));
        		p.setNombre(rs.getString(2));
        		p.setSemestre(rs.getInt(3));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}

}

















//
//SELECT p.cedula, p.nombre, p.apellido FROM persona p inner join aluasig m on p.cedula=m.alumno inner join asignatura q on m.asignatura=q.id where q.id=6;
