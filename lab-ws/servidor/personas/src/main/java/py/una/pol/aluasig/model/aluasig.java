package py.una.pol.aluasig.model;

import java.io.Serializable;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement

public class aluasig implements Serializable
{
	long id;
    long alumno;
    long asignatura;	
	
    public aluasig()
    {
    	
    }
    
    public aluasig(long id, long alumno, long asignatura) {
		super();
		this.id = id;
		this.alumno = alumno;
		this.asignatura = asignatura;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAlumno() {
		return alumno;
	}

	public void setAlumno(long alumno) {
		this.alumno = alumno;
	}

	public long getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(long asignatura) {
		this.asignatura = asignatura;
	}

    

}
