package py.una.pol.aluasig.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.aluasig.dao.AluAsigDAO;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.personas.model.Persona;

import java.util.List;
import java.util.logging.Logger;


@Stateless
public class AluasigService {
    @Inject
    private Logger log;

    @Inject
    private AluAsigDAO dao;
    
    public void relacionar(long cedula,long asignatura) throws Exception {
        log.info("Relacionando alumno: " + cedula+ " con asignatura: " +asignatura);
        try {
        	dao.relacionar(cedula, asignatura);
        }catch(Exception e) {
        	log.severe("ERROR al relacionar alumno: " + cedula+ " con asignatura: " +asignatura);
        	throw e;
        }
        log.info("Relacion creada con éxito: " + cedula + " - " + asignatura );
    }
    
    public List<Persona> listaralumnos(long asignatura) {
    	return dao.listaAlumnos(asignatura);
    }
    
    public List<Asignatura> listarasignaturas(long cedula) {
    	return dao.listaAsignaturas(cedula);
    }

}
