package py.una.pol.asignaturas.dao;


import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.swing.text.html.HTMLDocument.Iterator;

import py.una.pol.personas.dao.Bd;
import py.una.pol.personas.model.*;
import py.una.pol.asignaturas.*;
import py.una.pol.asignaturas.model.Asignatura;

@Stateless

public class AsignaturaDAO {

	
    @Inject
    private Logger log;
    
	public List<Asignatura> seleccionar() {
		String query = "SELECT id,nombre, semestre FROM asignatura ";
		
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);
       

        	while(rs.next()) {
        		Asignatura p = new Asignatura();
        		p.setId(rs.getInt(1));
        		p.setNombre(rs.getString(2));
        		p.setSemestre(rs.getInt(3));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}
	
	
	
	 public long insertar(Asignatura p) throws SQLException {

	        String SQL = "INSERT INTO asignatura(id,nombre,semestre) "
	                + "VALUES(?,?,?)";
	 
	        long id = 0;
	        Connection conn = null;
	        
	        try 
	        {
	        	
	        	conn = Bd.connect();
//	        	int[] aux=p.getAlumnos();
//	        	String arr[]=new String[aux.length];
//	        	for(int i=0;i<aux.length;i++)
//	        	{
//	        	arr[i]=String.valueOf(aux[i]);
//	        	}
//	        	java.sql.Array a = conn.createArrayOf("INTEGER", arr);
	 

	        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
	        	pstmt.setInt(1, p.getId());
	        	pstmt.setString(2, p.getNombre());
	            pstmt.setInt(3, p.getSemestre());
//	            pstmt.setArray(3,a);

	            
	 
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        id = rs.getInt(1);
	                    }
	                } catch (SQLException ex) {
	                	throw ex;
	                }
	            }
	        } catch (SQLException ex) {
	        	throw ex;
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
	        	
	        return id;
	    	
	    	
	    }
		

	    public long actualizar(Asignatura p) throws SQLException {

	        String SQL = "UPDATE asignatura SET nombre = ? , semestre = ?  WHERE id = ? "; 
	        long id = 0;
	        Connection conn = null;
	        
	        try 
	        {
	        	conn = Bd.connect();
//	        	int[] aux=p.getAlumnos();
//	        	String arr[]=new String[aux.length];
//	        	for(int i=0;i<aux.length;i++)
//	        	{
//	        	arr[i]=String.valueOf(aux[i]);
//	        	}
//	        	java.sql.Array a = conn.createArrayOf("INTEGER", arr);
	 

	        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
	            pstmt.setString(1, p.getNombre());
	            pstmt.setInt(2, p.getSemestre());	            
	            pstmt.setInt(3,p.getId() );
	 
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        id = rs.getInt(3);
	                    }
	                } catch (SQLException ex) {
	                    System.out.println(ex.getMessage());
	                }
	            }
	        } catch (SQLException ex) {
	        	log.severe("Error en la actualizacion: " + ex.getMessage());
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
	        return id;
	    }
	    
	    
	    public long borrar(int id) throws SQLException {

	        String SQL = "DELETE FROM asignatura WHERE id = ? ";
	 
	        long ID = 0;
	        Connection conn = null;
	        
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL);
	            pstmt.setInt(1, id);
	 
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        ID = rs.getInt(1);
	                    }
	                } catch (SQLException ex) {
	                	log.severe("Error en la eliminación: " + ex.getMessage());
	                	throw ex;
	                }
	            }
	        } catch (SQLException ex) {
	        	log.severe("Error en la eliminación: " + ex.getMessage());
	        	throw ex;
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        		throw ef;
	        	}
	        }
	        return ID;
	    }
	    
	    
		public void ListarAlumnos(String nombre) {
			String SQL = "SELECT alumnos FROM asignatura WHERE nombre = ? ";
			
			Asignatura p = null;
			
			Connection conn = null; 
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL);
	        	pstmt.setString(1, nombre);
	        	
	        	ResultSet rs = pstmt.executeQuery();
	        	
	        	List<Integer> al = new ArrayList<Integer>();


	        	while(rs.next()) {
	        		p = new Asignatura();
//	        		p.setId(rs.getInt(1));
//	        		p.setSemestre(rs.getInt(2));
	        		java.sql.Array aux=rs.getArray(1);
	        		
	        		
	        	    for (Object obj : (Object[])aux.getArray()) {
	        	        try {
	        	            Integer arr = (Integer) obj;
	        	            al.add(arr);
	        	        } catch (ClassCastException e) {
	        	            System.out.println("Object is not a String[]");
	        	            e.printStackTrace();
	        	        }
	        	    }
	        	    System.out.println("       Alumnos inscriptos en la materia " + nombre+" :");
	        	    System.out.println("Cedula: ");
	        	    for (int j = 0; j < al.size(); j++)
	                {
	                    System.out.println("       "+ al.get(j));
	                }
	        	    
	        	    
	        	}
	        	
	        } catch (SQLException ex) {
	        	log.severe("Error en la seleccion: " + ex.getMessage());
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }			

		}
		
		
		
		public Asignatura seleccionarPorID(long id) {
			String SQL = "SELECT  id,nombre,semestre FROM asignatura WHERE id = ? ";
			
			Asignatura p = null;
			
			Connection conn = null; 
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL);
	        	pstmt.setLong(1, id);
	        	
	        	ResultSet rs = pstmt.executeQuery();

	        	while(rs.next()) {
	        		p = new Asignatura();
	        		p.setId(rs.getInt(1));
	        		p.setNombre(rs.getString(2));
	        		p.setSemestre(rs.getInt(3));
	        	}
	        	
	        } catch (SQLException ex) {
	        	log.severe("Error en la seleccion: " + ex.getMessage());
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
			return p;

		}
		
		public Asignatura seleccionarPorNombre(String nombre) {
			String SQL = "SELECT  id,semestre, alumnos FROM asignatura WHERE nombre = ? ";
			
			Asignatura p = null;
			
			Connection conn = null; 
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL);
	        	pstmt.setString(1, nombre);
	        	
	        	ResultSet rs = pstmt.executeQuery();

	        	while(rs.next()) {
	        		p = new Asignatura();
	        		p.setId(rs.getInt(1));
	        		p.setSemestre(rs.getInt(2));
	        	}
	        	
	        } catch (SQLException ex) {
	        	log.severe("Error en la seleccion: " + ex.getMessage());
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
			return p;

		}
	    

}
