package py.una.pol.asignaturas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.asignaturas.dao.AsignaturaDAO;
import py.una.pol.asignaturas.model.Asignatura;
import py.una.pol.personas.model.Persona;

import java.util.List;
import java.util.logging.Logger;


@Stateless
public class AsignaturaService {
	
    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;
    
    
    public void crear(Asignatura p) throws Exception {
        log.info("Creando asignatura: " + p.getId()+ " " +p.getNombre() + " " +p.getSemestre());
        try {
        	dao.insertar(p);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + p.getNombre() + " " + p.getSemestre() );
    }
    
    
    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public void ListarALumnos(String nombre) {
    	dao.ListarAlumnos(nombre);
    }
    
    public long actualizar(Asignatura p) throws Exception{
        return dao.actualizar(p);
    }
    
    public long borrar(int id) throws Exception {
    	return dao.borrar(id);
    }
    
    public Asignatura seleccionarPorID(int id) {
    	return dao.seleccionarPorID(id);
    }
    
    public Asignatura seleccionarPorNombre(String nombre) {
    	return dao.seleccionarPorNombre(nombre);
    }
}
