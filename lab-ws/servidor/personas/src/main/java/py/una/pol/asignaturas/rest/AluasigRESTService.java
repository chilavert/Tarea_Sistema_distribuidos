package py.una.pol.asignaturas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.aluasig.model.aluasig;
import py.una.pol.aluasig.service.AluasigService;
import py.una.pol.personas.model.Persona;
import py.una.pol.asignaturas.model.Asignatura;


@Path("/relacionar")
@RequestScoped
public class AluasigRESTService {
    @Inject
    private Logger log;

    @Inject
    AluasigService aluasigService;
    
    @GET
    @Path("/relacionar")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response relacionar(@QueryParam("cedula") long cedula,@QueryParam("id") long id) {
        Response.ResponseBuilder builder = null;
        try
        {
        	aluasigService.relacionar(cedula, id);
        	
            builder = Response.status(201).entity("Relacion realizada exitosamente");
        }catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();   
    }
    
    
    @GET
    @Path("/{asignatura}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Persona> listarAlumnos(@QueryParam("asignatura")long asignatura) {
        return aluasigService.listaralumnos(asignatura);
    }
    
    @GET
    @Path("/listar/{cedula}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Asignatura> listarAsignaturas(@QueryParam("cedula")long cedula) {
        return aluasigService.listarasignaturas(cedula);
    }


}
